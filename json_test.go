package gojson

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"path/filepath"
	"testing"
)

// structs para json de prueba
type usuario struct {
	Id int `json:"id"`
	Nombre string `json:"nombre"`
	Articulos []articulo `json:"articulos"`
}
type articulo struct {
	Id int `json:"id,string"`
	Articulo string `json:"articulo"`
}
type respuesta struct {
	Error bool `json:"error"`
	Mensaje string `json:"mensaje"`
}

func TestJsonAStruct(t *testing.T) {
	u := usuario{}
	err := DecodificarArchivo(filepath.Join("json_test.json"), &u)
	if err != nil {
		t.Error("ERROR: ", err)
	}
	t.Log("Usuario: ", u)
}

////////////  test DecodificarRequestBody DecodificarString ////////////////

func pruebaBodyHandler(w http.ResponseWriter, r *http.Request) {
	a := articulo{}
	resp := respuesta{}
	var e []byte
	err := DecodificarRequestBody(r, &a)
	if resp.Error = err != nil; resp.Error {
		w.WriteHeader(http.StatusInternalServerError)
		w.Header().Set(	"Content-Type", "application/json")
		resp.Mensaje = err.Error()
	} else {
		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "application/json")
		resp.Mensaje = fmt.Sprintf("id=%d nombre=%s", a.Id, a.Articulo)
	}
	e, _ = json.Marshal(resp)
	_, _ = w.Write(e)
}

func pruebaParamsHandler(w http.ResponseWriter, r *http.Request) {
	a := articulo{}
	resp := respuesta{}
	var e []byte
	err := DecodificarRequestParams(r, &a)
	if resp.Error = err != nil; resp.Error {
		w.WriteHeader(http.StatusInternalServerError)
		w.Header().Set(	"Content-Type", "application/json")
		resp.Mensaje = err.Error()
	} else {
		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "application/json")
		resp.Mensaje = fmt.Sprintf("id=%d nombre=%s", a.Id, a.Articulo)
	}
	e, _ = json.Marshal(resp)
	_, _ = w.Write(e)
}

func TestBodyStringAStruct(t *testing.T) {
	req, err := http.NewRequest("POST", "/prueba", bytes.NewBuffer([]byte(` {
      "id": "1",
      "articulo": "articulo 1"
    }`)))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(pruebaBodyHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Error al realizar la solicitud %v", status)
	}

	resp := respuesta{}
	err = DecodificarString(rr.Body.String(), &resp)

	if err != nil {
		t.Errorf("Error en recepcion %v", err)
	}
	t.Logf("Respuesta: %v", resp)
}

func TestParamsAStruct(t *testing.T) {
	req, err := http.NewRequest("GET", "/prueba", nil)
	if err != nil {
		t.Fatal(err)
	}
	q := req.URL.Query()
	q.Add("id", "2")
	q.Add("articulo", "a2")
	req.URL.RawQuery = q.Encode()

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(pruebaParamsHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Error al realizar la solicitud %v", status)
	}

	resp := respuesta{}
	err = DecodificarString(rr.Body.String(), &resp)

	if err != nil {
		t.Errorf("Error en recepcion %v", err)
	}
	t.Logf("Respuesta: %v", resp)
}

func TestAJson(t *testing.T) {
	// maps
	m := make(map[string]int)
	m["uno"] = 1
	m["dos"] = 2
	m["dos"] = 323 // toma el ultimo valor, si la clave si duplica
	js, err := Codificar(m)
	if err != nil {
		t.Errorf("Error en conversion de map %v", err)
	}
	t.Log("map: ", js)

	//struct
	s := struct {
		Clave string
		Valor int
	}{
		"uno",
		1,
	}
	js, err = Codificar(s)
	if err != nil {
		t.Errorf("Error en conversion de struct %v", err)
	}
	t.Log("struct: ", js)
}
