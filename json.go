/*
*  JSON
*  Conjunto de funciones para leer archivos json y mapearlos en structs para operarlos en go
*  Author MALDRU
*  Copyright (c) 2019. All rights reserved.
*
 */

package gojson

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
)

/*
DecodificarArchivo mapea un archivo json a un tipo indicado
Ruta: ruta relativa o absoluta incluyendo el archivo json
modelo: puntero de tipo en donde se decodificara el contenido del archivo
*/
func DecodificarArchivo(ruta string, modelo interface{}) error {
	js, err := os.Open(ruta)
	if err == nil {
		defer js.Close()
		deco := json.NewDecoder(js)
		err = deco.Decode(&modelo)
	}
	return err
}

/*
DecodificarRequestBody mapea una peticion con body json a un struct
r: peticion del cliente
modelo: struct en donde se decodificara el cuerpo de la peticion
*/
func DecodificarRequestBody(r *http.Request, modelo interface{}) error {
	jsn, err := ioutil.ReadAll(r.Body)
	if err == nil {
		err = json.Unmarshal(jsn, &modelo)
	}
	return err
}

/*
DecodificarRequestParams mapea una peticion con params json a un struct
r: peticion del cliente
modelo: struct en donde se decodificara los parametros de la peticion
*/
func DecodificarRequestParams(r *http.Request, modelo interface{}) error {
	params := r.URL.Query().Encode()
	replacer := strings.NewReplacer("&", "\",\"", "=", "\":\"")
	return json.Unmarshal([]byte(`{"`+replacer.Replace(params)+`"}`), &modelo)
}

/*
DecodificarString mapea un string json a un struct
js: string json
modelo: struct en donde se decodificara el string
*/
func DecodificarString(js string, modelo interface{}) error {
	return json.Unmarshal([]byte(js), &modelo)
}

/*
Codificar mapea interface a json (string)
js: string json
modelo: struct en donde se decodificara el string
*/
func Codificar(datos interface{}) (js string, err error) {
	conversion, err := json.Marshal(datos)
	if err != nil {
		return js, err
	}
	return string(conversion), err
}
