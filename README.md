## GO JSON
Conjunto de funciones para leer archivos json y mapearlos en interface definidias para operarlos en go

### Instalacion 

    go get -u gitlab.com/ing.andres.latorre/gojson

**Nota:** Actualmente el repositorio es privado, solo con permisos sobre este se podria realizar el clonado o obtencion del mismo.
 
Para los usuarios con acceso a este, deben marcar el dominio como privado creando una variable de entorno llamada **GOPRIVATE** y con el valor: **gitlab.com/ing.andres.latorre/***

Las credenciales para la autenticacion deben ser mediante **HTTP** y **NO** por SSH.

### Uso

### Ejemplos:
Los ejemplos se pueden tomar de las pruebas unitarias [Test](https://gitlab.com/ing.andres.latorre/gojson/-/blob/master/json_test.go)

### Licencia    
Esta obra está licenciada bajo la Licencia Creative Commons Atribución-NoComercial-SinDerivadas 4.0 Internacional. Para ver una copia de esta licencia, visite http://creativecommons.org/licenses/by-nc-nd/4.0/.
